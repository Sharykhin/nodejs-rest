#### Requirements:
- docker

#### Usage:
1. Build images
```bash
docker-compose build
```

2. Run development with no debug:
```bash
docker-compose up rest postgres
```
Run with debug:
```bash
docker-compose up rest-debug postgres
```