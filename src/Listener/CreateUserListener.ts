import {Event} from './../Event/Event'

export class CreateUserListener {
    public handle(event: Event): void {
        console.log('User created:', event);
    }
}
