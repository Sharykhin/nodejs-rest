import * as express from 'express'
import router from './router'
import * as bodyParser from "body-parser"
import {initSubscribers} from './subscriber' 

const port = process.env.PORT || 3000
const app = express()

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', router)
app.listen(port, (err) => {
    if (err) {
        return console.error(err)
    }
    initSubscribers()
    return console.log(`server is listening on ${port}`)
})
