import {SequelizeUserRepository} from '../Domain/User/Repository/SequelizeUserRepository'

const userRepository = new SequelizeUserRepository()

export { 
    userRepository,
 }
