import {userRepository} from './Repository'
import {UserServiceHandler} from '../Domain/User/Service/UserServiceHandler'
import {LocalDispatcher} from './../Event/Event'
import {userService as userServiceImpl} from '../Domain/User';

const dispatcher = new LocalDispatcher();
//TODO: it should not work cause the contract is not setisfied but it works, shit.
const userService = new UserServiceHandler(userRepository, dispatcher)

export { 
    userService,
    dispatcher,
    userServiceImpl,
}
