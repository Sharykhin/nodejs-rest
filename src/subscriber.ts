import {dispatcher} from './Provider/Service'
import {Events, Event} from './Event/Event'
import {createUserListener} from './Provider/Listener'

export function initSubscribers() {
    dispatcher.on(Events.USER_CREATED, createUserListener.handle)
}
