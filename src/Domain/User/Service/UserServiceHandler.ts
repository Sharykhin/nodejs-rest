import {IUserRepository, UserInteface} from '../Repository/IUserRepository';
import {UserRequest} from './IUserService';
import {User} from '../Model';
import {IDispatcher, Events} from '../../../Event/Event';
import { ValidationError } from '../../../Exception/ValidationError';

export class UserServiceHandler {
    
    protected repository: UserInteface;

    protected dispatcher: IDispatcher;
    
    public constructor(repository: UserInteface, dispatcher: IDispatcher) {
        this.repository = repository;
        this.dispatcher = dispatcher;
    }

    public async create(request: UserRequest): Promise<User> {
        try {
            this.validateOnCreate(request)
        } catch(e) {
            console.log('validation failed');

            throw e;
        }
        
        const id = await this.repository.create(request.name, request.email)
        const aggregate =  new User(id, request.name, request.email);
        this.dispatcher.dispatch(Events.USER_CREATED, {
            payload: {
                id: id,
            }
        })
        
        return aggregate;
    }

    protected validateOnCreate(request: UserRequest): void {
        if (request === undefined) {
            throw new ValidationError('request is missing');
        }

        if (request.name === undefined || request.name.trim() === '') {
            throw new ValidationError('name is required');
        }

        if (request.email === undefined  || request.email.trim() === '') {
            throw new ValidationError('email is required');
        }
    }
}
