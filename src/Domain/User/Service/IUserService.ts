import {User} from './../Model';

export abstract class IUserService {
    abstract async create(request: UserRequest): Promise<User>
}

export interface UserRequest {
    name: string
    email: string
}
