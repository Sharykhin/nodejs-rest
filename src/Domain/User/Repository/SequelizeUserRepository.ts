import {User} from './User';
import { Sequelize, Model, DataTypes, BuildOptions } from "sequelize";
import {database} from './../../../config'
import { IUserRepository } from './IUserRepository';

class SequelizeUserModel extends Model {
  public id: number;
  public name: string;
  public email: string
}

SequelizeUserModel.init({
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataTypes.INTEGER
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false
  },
  createdAt: {
    allowNull: false,
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    type: DataTypes.DATE,
  },
},
  {
    tableName: "users",
    sequelize: database // this bit is important
  }
);

export class SequelizeUserRepository {

  public async create(name: string, email: string): Promise<number> {
    const user = await SequelizeUserModel.create({
        name: name,
        email: email
    })

    return user.id
  }

  public findByID (ID: string): Promise<User> {
    return new Promise((resolve) => {
      setTimeout(() => {
        console.log('find by id resolved');
        resolve(new User(ID, 'Sergey', 'chapal@inbox.ru'));
      }, 1000);
    });
  }
}
