import {User} from './User';

export abstract class IUserRepository {
    abstract findByID(ID: string): Promise<User>;
    abstract async create(name: string, email: string, age: number): Promise<string>;
}

//TODO: experimental usage
export interface UserInteface {
    findByID(ID: string): Promise<User>;
    create(name: string, email: string, age: number): Promise<number>;
}
