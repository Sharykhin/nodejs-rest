import {SequelizeUserRepository} from './Repository/SequelizeUserRepository'
import {UserServiceHandler} from './Service/UserServiceHandler'
import {LocalDispatcher} from '../../Event/Event'

const userRepository = new SequelizeUserRepository()
const dispatcher = new LocalDispatcher();
const userService = new UserServiceHandler(userRepository, dispatcher)

export { 
    userService,
 }
