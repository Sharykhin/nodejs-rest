import * as express from 'express'
import {UserHttpHandler} from './Http/User'
import {userService} from './Provider/Service'

const router = express.Router();

router.get('/', (req, res) => {
    res.json({
        message: 'Hello World!'
    });
});

router.post('/users', (req, res) => {
    UserHttpHandler.create(userService, req, res);
})

export default router