
export interface Event {
    payload: object
}

export interface IDispatcher {
    on(eventName: string, listener: (event: Event) => void): void
    dispatch(eventName: string, event: Event): void
}

export const Events = {
    USER_CREATED: 'userCreated',
}

export class LocalDispatcher {
    protected subscribers = [];

    public on(eventName: string, listener: (event: Event) => void): void {
        const subscribers = this.subscribers[eventName] || [];
        subscribers.push(listener);

        this.subscribers[eventName] = subscribers;
    }

    public dispatch(eventName: string, event: Event): void {
        for (let listener of this.subscribers[eventName] || []) {
            setTimeout(() => {
                listener(event);
            }, 1000);
        }
    }
}
