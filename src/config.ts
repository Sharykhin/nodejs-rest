import { Sequelize } from "sequelize";

//TODO: take eveything from env
export const database = new Sequelize({
    "username": "postgres",
    "password": "root",
    "database": "noderest",
    "host": "postgres",
    "port": 5432,
    "dialect": "postgres"
  });