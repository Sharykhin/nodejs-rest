import {IUserService} from './../Domain/User/Service/IUserService';
import {ValidationError } from '../Exception/ValidationError';
import {Request, Response} from "express";

export class UserHttpHandler {
    public static async create(userService: IUserService, req: Request, res: Response) {
        console.log('run <User.index> http handler. Request body:', req.body)
        try {
            const user = await userService.create(req.body);
            console.log('response <User.index> http handler:', user)
            res.status(201);
            res.json(user);
        } catch (e) {
            res.status(e instanceof ValidationError ? 400 : 500);
            const response = {
                error: e.message
            }
            console.log('response <User.index> http handler:', response)
            res.json(response);
        }
    }
};
